import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ArrayList<E> implements List<E> {

	private E[] array;
	private int size;
	private int capacity;
	
	public ArrayList() {
		this(20);
	}

	public ArrayList(int initialCapacity) {
		capacity = initialCapacity;
		array = (E[]) new Object[initialCapacity];
	}
	
	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public boolean contains(Object o) {
		
		if (o == null) {
			return false;
		}
		
		for (E e : array) {
			if (o.equals(e)) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public Object[] toArray() {
		return array;
	}

	@Override
	public boolean add(E e) {
		
		array[size] = e;
		size++;
		
		if (size == capacity) {
			resize();
		}
		
		return true;
	}
	
	private void resize() {
		
		capacity = capacity * 2;
		E[] newArray = (E[]) new Object[capacity];
		
		for (int i = 0; i < size; i++) {
			newArray[i] = array[i];
		}
		
		array = newArray;
	}

	@Override
	public boolean remove(Object o) {
		
		if (o == null) {
			return false;
		}
		
		boolean didRemove = false;
		
		for (int i = 0; i < size; i++) {
			if (o.equals(array[i])) {
				remove(i);
				didRemove = true;
				
				// make i go back one because everything shifts one spot to the left.
				i--;
			}
		}
		
		return didRemove;
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		
		for(E item : c) {
			if (!add(item)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void clear() {
		size = 0;
		capacity = 20;
		array = (E[]) new Object[capacity];
	}

	@Override
	public E get(int index) {
		
		if (index >= size) {
			throw new IndexOutOfBoundsException("Specified index is out of bounds.");
		}
		
		return array[index];
	}

	@Override
	public E set(int index, E element) {
		
		if (index >= size) {
			throw new IndexOutOfBoundsException("Specified index is out of bounds.");
		}
		
		E oldElement = array[index];
		array[index] = element;
		
		return oldElement;
	}

	@Override
	public void add(int index, E element) {
		
		if (index >= size) {
			throw new IndexOutOfBoundsException("Specified index is out of bounds.");
		}
		
		for (int i = size - 1, j = size; i >= index; i--, j--) {
			array[j] = array[i];
		}
		
		array[index] = element;
		size++;
		
		if (size == capacity) {
			resize();
		}
	}

	@Override
	public E remove(int index) {
		if (index >= size) {
			throw new IndexOutOfBoundsException("Specified index is out of bounds");
		}
		
		E element = array[index];
		
		if (index == size - 1) {
			array[index] = null;
		} else {
			for (int i = index, j = index + 1; j < size; i++, j++) {
				array[i] = array[j];
			}
			
			array[size - 1] = null;
		}
		
		size--;
		
		return element;
	}

	@Override
	public int indexOf(Object o) {
		
		if (o == null) {
			throw new IllegalArgumentException("Expecting a non-null argument");
		}
		
		for (int i = 0; i < size; i++) {
			if (o.equals(array[i])) {
				return i;
			}
		}
		
		return -1;
	}

	@Override
	public int lastIndexOf(Object o) {
		
		if (o == null) {
			throw new IllegalArgumentException("Expecting a non-null argument");
		}
		
		for (int i = size - 1; i >= 0; i++) {
			if (o.equals(array[i])) {
				return i;
			}
		}
		
		return -1;
	}

	
	
	
	// ------------------------------------------------------
	// ------------------------------------------------------
	// The below are methods that we don't care to implement.
	// ------------------------------------------------------
	// ------------------------------------------------------


	@Override
	public Iterator<E> iterator() {
		return null;
	}
	
	@Override
	public <T> T[] toArray(T[] a) {
		return null;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return false;
	}
	
	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return false;
	}
	
	@Override
	public ListIterator<E> listIterator() {
		return null;
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return null;
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		return null;
	}
}
