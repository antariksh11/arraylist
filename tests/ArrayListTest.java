import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ArrayListTest {

	private ArrayList<String> list;
	
	@BeforeEach
	public void Init() {
		list = new ArrayList<String>();
	}
	
	@Test
	public void testAddOneItem() {
		list.add("Apples");
		
		Object[] underlyingArray = list.toArray();
		
		assertEquals(20, underlyingArray.length, "The capacity of the array should have been 20");
		assertEquals("Apples", underlyingArray[0], "The element inside the list should have been \"Apples\".");
		
		for (int i = 1; i < underlyingArray.length; i++) {
			assertNull(underlyingArray[i], String.format("Element at index %d should be null", i));
		}
	}
	
	@Test
	public void testAddMultipleItems() {
		list.add("Apples");
		list.add("Oranges");
		list.add("Bananas");

		Object[] underlyingArray = list.toArray();
		
		assertEquals(20, underlyingArray.length, "The capacity of the array should have been 20");
		assertEquals("Apples", underlyingArray[0], "The element inside the list should have been \"Apples\".");
		assertEquals("Oranges", underlyingArray[1], "The element inside the list should have been \"Oranges\".");
		assertEquals("Bananas", underlyingArray[2], "The element inside the list should have been \"Bananas\".");
		
		for (int i = 3; i < underlyingArray.length; i++) {
			assertNull(underlyingArray[i], String.format("Element at index %d should be null", i));
		}
	}
	
	@Test
	public void testAddResize() {
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		list.add("Apples");
		
		Object[] underlyingArray = list.toArray();
		
		assertEquals(40, underlyingArray.length, "The underlying array should have resized to double the initial value should have had a length of 40.");
		
		for (int i = 0; i < underlyingArray.length / 2; i++) {
			assertEquals("Apples", underlyingArray[i], String.format("The value at index %d should have been \"Apples\".", i));
		}
		
		for (int i = 20; i < underlyingArray.length; i++) {
			assertNull(underlyingArray[i], String.format("The value at index %d should have been null.", i));
		}
	}
	
	@Test
	public void testSize() {
		list.add("");
		
		assertEquals(1, list.size());
		
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		list.add("");
		
		assertEquals(22, list.size());
	}
	
	@Test
	public void testIsEmpty() {
		assertTrue(list.isEmpty());
		
		list.add("");
		
		assertFalse(list.isEmpty());
	}
	
	@Test
	public void testContainsWhenListIsEmpty() {
		assertFalse(list.contains("Test"));
	}
	
	@Test
	public void testContainsWhenNullIsSearchedFor() {
		assertFalse(list.contains(null));
	}
	
	@Test
	public void testContainsWhenElementIsInList() {
		list.add("Apples");
		list.add("Oranges");
		list.add("Bananas");
		
		assertTrue(list.contains("Apples"));
	}
	
	@Test
	public void testContainsWhenElementIsNotInList() {
		list.add("Apples");
		list.add("Oranges");
		list.add("Bananas");
		
		assertFalse(list.contains("Mango"));
	}

	@Test
	public void testRemoveObject() {
		list.add("Apples");
		list.add("Oranges");
		list.add("Bananas");
		
		list.remove("Oranges");
		
		assertFalse(list.contains("Oranges"));
	}
	
	@Test
	public void testRemoveObjectWhenNullIsAttemptedToBeRemoved() {
		assertFalse(list.remove(null));
	}
	
	@Test
	public void testRemoveObjectWhenMultipleObjectsAreToBeRemoved() {
		list.add("Apples");
		list.add("Bananas");
		list.add("Bananas");
		
		list.remove("Bananas");
		
		assertFalse(list.contains("Bananas"));
	}
	
	@Test
	public void testGetWithIndexOutOfBounds() {
		try {
			list.get(0);
			assertTrue(false, "IndexOutOfBoundsException should have been thrown.");
		} catch (IndexOutOfBoundsException ex) {
			assertEquals("Specified index is out of bounds.", ex.getMessage());
		}
	}
	
	@Test
	public void testGet() {
		list.add("Apples");
		list.add("Oranges");
		list.add("Bananas");
		
		Object[] array = list.toArray();
		
		for (int i = 0; i < list.size(); i++) {
			assertEquals(list.get(i), array[i]);
		}
		
		for (int i = list.size(); i < array.length; i++) {
			assertNull(array[i]);
		}
	}
	
	@Test
	public void testRemoveHasNoHoles() {
		list.add("Apples");
		list.add("Oranges");
		list.add("Bananas");
		
		assertEquals(3, list.size());
		
		list.remove("Apples");
		
		assertFalse(list.contains("Apples"));
		assertEquals(2, list.size());
		
		Object[] array = list.toArray();
		
		assertEquals("Oranges", array[0]);
		assertEquals("Bananas", array[1]);
		assertNull(array[2]);
	}
}
